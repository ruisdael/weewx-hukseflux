# Hukseflux-Weewx Driver

*Source-code for the Hukseflux-Weewx driver[^1]*.

The Hukseflux-Weewx driver acquires data from a Hukseflux NR01 radiometers via the GPIO pins of a RaspberryPi.[^2]

In order to run Weewx with the Hukseflux driver:
* Weewx must be installed pip and a Python virtual environment, see how in [Weewx documentation:Installation using pip](https://www.weewx.com/docs/5.0/quickstarts/pip/)


## Hardware requirement
* Hukseflux NR01 radiometers [^3]
* ADC Differential Pi: 8-Channel Analogue to Digital Converter [^4]
* Hukseflux connection board (The 12V source power the raspberry pi as well)

## RasberryPi OS Requirements

**Enable the Pi's I2C[^5] interface:**
* with raspi-config: 
    * `sudo raspi-config`
    * in `raspi-config` menu choose: "5 Interfacing Options"
    * choose "P5 I2C" and enable it
    * reboot the Pi
* Or by editing `config.txt` [^6] (Prior to Bookworm, Raspberry Pi OS stored the boot partition at /boot/. Since Bookworm, the boot partition is located at `/boot/firmware/`)
    * uncomment the line: `dtparam=i2c_arm=on`
    * reboot

**Install the `i2c-tools`**, so that we can interact with the I2C devices
* `sudo apt install i2c-tools` - `i2c-tools`

**Check the components connected to `i2c`:** `sudo i2cdetect -y 1`


## Python Requirements
**Create a virtual-enviroment to for weewx**

**Install the driver's Python requirements:**
* smbus2: `pip install smbus2` to interact with I2C devices
* ADCDifferentialPi: `pip install git+https://github.com/abelectronicsuk/ABElectronics_Python_Libraries.git` to interact with *AB Electronics* board 

## Install the extension

Package this extension:
`tar cvfz hukselfux.tar.gz weewx-hukseflux/`

**With weewx virtual enviroment enabled**,<br/> install driver via weectl:
`weectl extension install hukselfux.tar.gz -y --verbosity=4`

Check if the extension is installed: `weectl extension list`


## Run Weewx with Hukselfux driver
Still with the virtual-environment active, run `weewx` with the right config file[^7]
`weewxd --conf weewx_Hukseflux_example.conf`

Note: location of the config file might be different for your installation

### Run as a service

Create symlink from the chosen Hukseflux config to `/etc/weewx/Hukseflux.conf`: `ln -s /usr/local/src/davis-ws-datalogger/configs_hukseflux/weewx_Hukseflux_davis001.conf /etc/weewx/Hukseflux.conf`

Copy service file to `/etc/systemd/system/`: `cp weewx-Hukseflux.service /etc/systemd/system/weewx-Hukseflux.service`

Enable service: `systemctl enable weewx-Hukseflux.service`

Start service: `systemctl start weewx-Hukseflux.service`

Check the service's status: `systemctl status weewx-Hukseflux.service`

Look at the logs, to check what is happening: `journalctl -f -u weewx-Hukseflux.service`


## Run Weewx with Vantage driver, as a service

Create symlink from the chosen Hukseflux config to `/etc/weewx/weewx.conf`: `ln -s /usr/local/src/davis-ws-datalogger/configs_weewx/weewx_davis_001.conf /etc/weewx/weewx.conf`

Copy service file to `/etc/systemd/system/`: `cp weewx-Vantage.service /etc/systemd/system/weewx-Vantage.service`

Enable service: `systemctl enable weewx-Vantage.service`

Start service: `systemctl start weewx-Vantage.service`

Check the service's status: `systemctl status weewx-Vantage.service`

Look at the logs, to check what is happening: `journalctl -f -u weewx-Vantage.service`




## Uninstall extension

`weectl extension uninstall hukselfux`


# Default locations

With the extension installed, and the a config file, similar to [weewx_Hukseflux_example.conf](weewx_Hukseflux_example.conf), you should expect for 
* the extension to be installed in: `/etc/weewx/bin/user/hukseflux.py`
* the hukseflux sqlite3 database to be located: `/etc/weewx/archive/hukseflux.sdb`



----


# Notes
[^1]: Official Weewx documentation on developing Weewx drivers: https://www.weewx.com/docs/5.0/custom/drivers/?h=driver

[^2]: Some examples of other Weewx drivers:
* [weewx-saratoga](https://github.com/gjr80/weewx-saratoga)
* [weewx-klimalogg](https://github.com/matthewwall/weewx-klimalogg)

[^3]: Hukseflux NR01 radiometers documentation: [NR01 user manual](https://www.hukseflux.com/uploads/product-documents/NR01_RA01_manual_v2303.pdf)

[^4]: ADC Differential Pi manufacturer information: [AB Electronics](https://www.abelectronics.co.uk/p/65/adc-differential-pi)

[^5]: Raspberry Pi supports the [I2C protocol](https://en.wikipedia.org/wiki/I%C2%B2C), allowing it to talk to a variety of I2C capable circuits

[^6]: Raspberry Pi official documentation on `config.txt`: https://www.raspberrypi.com/documentation/computers/config_txt.html

[^7]: See an example config file with the hukseflux driver at [weewx_Hukseflux_example.conf](weewx_Hukseflux_example.conf)