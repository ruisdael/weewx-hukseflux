# installer for hukseflux driver


from weecfg.extension import ExtensionInstaller

def loader():
    return HukselfluxInstaller()

class HukselfluxInstaller(ExtensionInstaller):
    def __init__(self):
        super(HukselfluxInstaller, self).__init__(
            version="0.2",
            name='hukseflux',
            description='Collects data from Hukseflux radiometer NR01',
            author=" Mahaut Sourzac ",
            author_email="m.sourzac@tudelft.nl",
            files=[('bin/user', ['bin/user/hukseflux.py',
                                 'bin/user/Hukseflux_Schema.py'])]
        )
