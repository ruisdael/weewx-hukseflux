'''
Weewx driver to collect and calculate data from a Hukseflux radiometer NR01
'''


import logging
import time
import math
import syslog
from ADCDifferentialPi import ADCDifferentialPi
import weewx.engine
import weewx.drivers
import weewx.units
from weeutil.weeutil import to_int, to_sorted_string
from weewx.crc16 import crc16  # is it needed?

log = logging.getLogger(__name__)

DRIVER_NAME = 'Hukseflux'
DRIVER_VERSION = '0.2'


def logmsg(level, msg):
    syslog.syslog(level, f'hukseflux: {msg}')


def logdbg(msg):
    logmsg(syslog.LOG_DEBUG, msg)


def loginf(msg):
    logmsg(syslog.LOG_INFO, msg)


def logerr(msg):
    logmsg(syslog.LOG_ERR, msg)


# def loader(config_dict, _):
#     return HuksefluxDriver(**config_dict[DRIVER_NAME])

def loader(config_dict, engine):
    return HuksefluxService(engine, config_dict)


###        Constants for the calculations        ###
# The bridge resistor values are:
resistor01 = 1000.0
resistor02 = 100.0
# Input voltage from the 5.0V Pin of the Pi.
voltIn = 5.0
# 0 deg Celsius in Kelvin.
t0 = 273.15
# Stefan Boltzmann constant for the calculation of the longwave irradiance.
stefanBoltzmann = 0.00000005670374419
# Constants for the calculation of the PT100 temperature.
alpha = 0.00385
beta = 0.00000058019

logdbg(msg=f'Running Weewx driver: {DRIVER_NAME}')


class HuksefluxDriver(weewx.drivers.AbstractDevice):
    '''
    Class that represent the connection to the ADC system connected to the Hukseflux radiometer.
    '''
    def __init__(self, **stn_dict):
        ''' Initialize an object of type Hukseflux 
        sample_rate : Sample rate can be 12, 14, 16 or 18 bits.
        I2Cline, I2Cadress_1, I2Cadress_2  : The I2C addresses can be found using the 'i2cdetect -y 1' command on the Pi terminal.
        sensitivity_solar_up, sensitivity_solar_down, sensitivity_longwave_up, sensitivity_longwave_down: Sensitivity measurement of the sensors
        interval : Interval used to mean the values in seconds [By default 60]
        num_samples_per_interval : Number of sample to collect per interval [By default 60]
        '''
        loginf(f'Hukseflux driver version {DRIVER_VERSION}')

        self.hardware_type = None

        # These come from the configuration dictionary:
        self.sample_rate = to_int(stn_dict.get('sample_rate')) 
        self.I2Cline = to_int(stn_dict.get('I2Cline'))
        self.I2Cadress_1 = stn_dict.get('I2Cadress_1')
        self.I2Cadress_2 = stn_dict.get('I2Cadress_2')
        self.sensitivity_solar_up = to_int(stn_dict.get('sensitivity_solar_up'))
        self.sensitivity_solar_down = to_int(stn_dict.get('sensitivity_solar_down'))
        self.sensitivity_longwave_up = to_int(stn_dict.get('sensitivity_longwave_up'))
        self.sensitivity_longwave_down = to_int(stn_dict.get('sensitivity_longwave_down'))
        self.interval = to_int(stn_dict.get('interval', 60))
        self.num_samples_per_interval = to_int(stn_dict.get('num_samples_per_interval', 60))
        I2Ca1 = str(self.I2Cline) + 'x' + str(self.I2Cadress_1)
        I2Ca2 = str(self.I2Cline) + 'x' + str(self.I2Cadress_2)

        # Initialise the ADC.
        self.adc = ADCDifferentialPi(int(I2Ca1, 16), int(I2Ca2, 16), self.sample_rate)

    def genLoopPackets(self):
        '''Generator function to return N loop packets from a ADC Differential Pi connected to the radiometer
        
        N: The number of packets to generate [default is 1]'''

        logdbg("Requesting the LOOP packets.")
        while True:
            # Read values from the Hukseflux radiometer via ADCDifferential Pi Hat
            # Compute irradiance values based on the temperature variable from the WeeWX database
            # Create a WeeWX packet with the computed values
            loop_packet = self.get_packet()
            yield loop_packet

    def get_data(self):
        ''' 1 -Read voltage values from the Hukseflux radiometer
            2 -Convert the PT100 voltage into resistance, then Convert PT100 resistance into temperature
            3 -Convert the raw differential voltage readings into irradiance values
            '''

        longwave_up_raw = self.adc.read_voltage(6)
        longwave_down_raw = self.adc.read_voltage(5)
        solar_up_raw = self.adc.read_voltage(2)
        solar_down_raw = self.adc.read_voltage(1)
        pt100_voltage = self.adc.read_voltage(8)

        thermoresistance = calc_resistance(pt100_voltage)
        temperature_radiometer = calc_temperature(thermoresistance)

        solar_up = calc_solar_irradiance(solar_up_raw, self.sensitivity_solar_up)
        solar_down = calc_solar_irradiance(solar_down_raw, self.sensitivity_solar_down)
        longwave_up = calc_longwave_irradiance(longwave_up_raw, self.sensitivity_longwave_up, temperature_radiometer)
        longwave_down = calc_longwave_irradiance(longwave_down_raw, self.sensitivity_longwave_down, temperature_radiometer)

        raw_data = {
            'longwave_up': longwave_up,
            'longwave_down': longwave_down,
            'solar_up': solar_up,
            'solar_down': solar_down,
            'RadiometerTemp': temperature_radiometer
        }
        return raw_data

    def get_packet(self):
        # Import the raw data then calculate the mean value for the define interval
        timer_ini = time.perf_counter()
        data = {
            'longwave_up': [],
            'longwave_down': [],
            'solar_up': [],
            'solar_down': [],
            'RadiometerTemp': []
        }
        
        for _ in range(self.num_samples_per_interval):
            raw_data = self.get_data()
            for var_name, value in raw_data.items():
                data[var_name].append(value)

        mean_values = calc_mean(data)

        loop_packet = {
            'dateTime': to_int(time.time()),
            'usUnits': weewx.US
        }

        for var_name, mean in mean_values.items():
            if mean is not None:
                loop_packet[var_name] = mean
            else:
                logerr("Error during the Hukseflux data processing")
        timer_end = time.perf_counter()
        logdbg(f'Took {timer_end - timer_ini:0.4f} seconds to create the loop_packet')

        return loop_packet

    @property
    def hardware_name(self):    
        return 'Hukseflux'

    @property
    def archive_interval(self):
        return self.interval
    
    def _determine_hardware(self):
        return 'Hukseflux'

    def _setup(self):
        """Unused"""
  
    def _getEEPROM_value(self, offset, v_format="B"):
        """Unused"""
        
#===============================================================================
#                      Hukseflux functions
#===============================================================================
def calc_resistance(voltage):
    # Calculate the Resistance PT100||resistor02 from the measured voltage and the Input voltage. (Voltage divider formula)
    PT100RES2 = ((voltage*resistor01)/(voltIn-voltage))
    # Then calculate the PT100 resistance
    return (((resistor02*PT100RES2)/(resistor02-PT100RES2)) + 2.2)

def calc_temperature(resistance):
    # Calculate the temperature from the PT100 resistance.
    return ((-alpha) + (math.sqrt((alpha * alpha) - ((4 * beta) * ((-resistance / 100) + 1))))) / (2 * beta)
    
def calc_solar_irradiance(solar_radiation, sensitivity):
    # Calculate the solar radiation from the SR01.
    return solar_radiation / (sensitivity*10e-6)

def calc_longwave_irradiance(longwave_radiation, sensitivity, temperature):
    #Calculate the longwave radiation from the IR01.
   return (longwave_radiation/(sensitivity*10e-6))+(stefanBoltzmann*((temperature+273.15)**4))

def calc_mean(data_dict):
    # Calculate the mean of the variables
    mean_values = {}
    for var_name, var_data in data_dict.items():
        if var_data:
            mean_values[var_name] = sum(var_data) / len(var_data)
        else:
            mean_values[var_name] = None
    return mean_values

#===============================================================================
#                      class HuksefluxService
#===============================================================================

# This class uses multiple inheritance:

class HuksefluxService(HuksefluxDriver, weewx.engine.StdService):
    """Weewx service for the Hukseflux radiometer."""
    
    def __init__(self, engine, config_dict):
        HuksefluxDriver.__init__(self, **config_dict[DRIVER_NAME])
        weewx.engine.StdService.__init__(self, engine, config_dict)

        self.net_radiation = None
        self.albedo = None
        self.Temp_surface = None
        self.Temp_sky = None

        self.bind(weewx.STARTUP, self.startup)        
        self.bind(weewx.NEW_LOOP_PACKET,    self.new_loop_packet)
        self.bind(weewx.END_ARCHIVE_PERIOD, self.end_archive_period)

    def startup(self, event):  # @UnusedVariable
        self.net_radiation = None
        self.albedo = None
        self.Temp_surface = None
        self.Temp_sky = None

    def closePort(self):
        # Now close my superclass's port:
        HuksefluxDriver.closePort(self)

    def new_loop_packet(self, event):
        """Calculate the net radiation, albedo, surface temperature and sky temperature from the last archive record."""
        longwave_up = event.packet.get('longwave_up')
        longwave_down = event.packet.get('longwave_down')
        solar_up = event.packet.get('solar_up')
        solar_down = event.packet.get('solar_down')

        self.net_radiation = (solar_down - solar_up) + (longwave_down - longwave_up)
        if solar_down == 0:
            self.albedo = None
        else:
            self.albedo = solar_up / solar_down
        self.Temp_surface = (longwave_up / stefanBoltzmann)**(1/4) - t0
        self.Temp_sky = (longwave_down / stefanBoltzmann)**(1/4) - t0
        
        event.packet['net_radiation'] = self.net_radiation
        event.packet['albedo'] = self.albedo
        event.packet['Temp_surface'] = self.Temp_surface
        event.packet['Temp_sky'] = self.Temp_sky       
        logdbg('Derivated values added to the packet')

    def end_archive_period(self, event):
        """Zero out"""
        self.net_radiation = None
        self.albedo = None
        self.Temp_surface = None
        self.Temp_sky = None


if __name__ == '__main__':
    driver = HuksefluxDriver()
