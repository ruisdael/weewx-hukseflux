"""A very small schema + radiometers data"""

# =============================================================================
# This is a very severely restricted schema that includes only the hukseflux 
# driver results. Like other WeeWX schemas, it is used only for initialization 
# --- afterwards, the schema is obtained dynamically from the database.  
# Although a type may be listed here, it may not necessarily be supported by 
# your weather station hardware.
# =============================================================================
# NB: This schema is specified using the WeeWX V4 "new-style" schema.
# =============================================================================
table = [('dateTime',             'INTEGER NOT NULL UNIQUE PRIMARY KEY'),
         ('usUnits',              'INTEGER NOT NULL'),
         ('interval',             'INTEGER NOT NULL'),
         ('longwave_up',          'REAL'),
         ('longwave_down',        'REAL'),
         ('solar_up',             'REAL'),
         ('solar_down',           'REAL'),
         ('RadiometerTemp',       'REAL'),
         ('net_radiation',        'REAL'),
         ('albedo',               'REAL'),
         ('Temp_surface',         'REAL'),
         ('Temp_sky',             'REAL')
         ]

day_summaries = [(e[0], 'scalar') for e in table
                 if e[0] not in ('dateTime', 'usUnits', 'interval')] + [('wind', 'VECTOR')]

schema = {
    'table': table,
    'day_summaries' : day_summaries
}
